#!/bin/bash

# # bash -c "$(curl -fsSL https://bitbucket.org/Wim_Roose/public/raw/master/upgrade_puppet_mac.sh)"

# https://puppet.com/docs/puppet/6/install_agents.html#install_mac_agents

MACOS_VERSION=$(sw_vers | grep ProductVersion | awk {'print $2'} | cut -d . -f 1)

echo $MACOS_VERSION
read -r -p 'press key to continue'

echo "Checking whether puppet installer needs to be downloaded..."
echo "downloading puppet for OSX $MACOS_VERSION"; 
curl https://downloads.puppet.com/mac/puppet7/$MACOS_VERSION/x86_64/puppet-agent-latest.dmg --output puppet-agent-latest.dmg

#cd /Volumes/$(ls /Volumes/puppet-agent-*)
mount | grep puppet || sudo hdiutil mount puppet-agent-latest.dmg
sudo installer -pkg $(ls -d /Volumes/puppet-*)/$(ls /Volumes/puppet-*) -target /
hdiutil unmount $(ls -d /Volumes/puppet*)

/opt/puppetlabs/bin/puppet --version
mv /opt/puppetlabs/puppet/bin/wrapper.sh /opt/puppetlabs/puppet/bin/wrapper
ln -sf /opt/puppetlabs/puppet/bin/wrapper /opt/puppetlabs/bin/facter
ln -sf /opt/puppetlabs/puppet/bin/wrapper /opt/puppetlabs/bin/hiera
ln -sf /opt/puppetlabs/puppet/bin/wrapper /opt/puppetlabs/bin/puppet
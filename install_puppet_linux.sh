#!/bin/bash

#set -e

# bash -c "$(curl -fsSL https://bitbucket.org/Wim_Roose/public/raw/master/install_puppet_linux.sh)"
CONFDIR="/etc/puppetlabs/puppet"
ARCH=$(uname -m)

if [ $ARCH == 'armv7l' ]; then
  echo "arm detected; installing ruby"
  apt list --installed | grep ruby 2>&1 > /dev/null
  if [ $? -eq 0 ];then
    echo "ruby already installed";
  else
    echo "ruby not found, installing"
    apt install ruby
  fi
  gem list -i "^puppet$" 2>&1 > /dev/null
  if [ $? -eq 1 ];then
    echo "installing puppet via gem"
    gem install puppet
  else
    echo "puppet already installed"
  fi
# regular Debian
else
  read -r -p "What version of puppet do you want? (6/7): " PUPPETVERSION
  DEBIANVERSION=$(lsb_release -c | awk {'print $2'})
  echo "Found $DEBIANVERSION, downloading repo config"
  wget "https://apt.puppet.com/puppet$PUPPETVERSION-release-$DEBIANVERSION.deb"
  dpkg -i "puppet$PUPPETVERSION-release-$DEBIANVERSION.deb"
  apt update
  apt install puppet-agent
fi

if [[ ! -d "$CONFDIR" ]]; then
  mkdir -p $CONFDIR
  cat <<EOF > "$CONFDIR/puppet.conf"
[main]
server = puppet.home
EOF
fi

[[ -f "$CONFDIR/puppet.conf" ]] && echo "Found existing puppet config, removing!"; rm -f "$CONFDIR/puppet.conf"

echo "Configuring puppet time!"

read -r -p "Certname: " CERTNAME
echo "certname= $CERTNAME" >> "$CONFDIR/puppet.conf"

read -r -p "IP address of puppet server: " IPADDRESS
sed -i 's/^.*puppet.*$//g' /etc/hosts
echo "$IPADDRESS puppet puppet.home" >> /etc/hosts

read -r -p "pp role: " PPROLE
read -r -p "pp env (prd/tst/dev): " PPENV
read -r -p "pp_zone: " PPZONE
cat <<EOF > "$CONFDIR/csr_attributes.yaml"
---
extension_requests:
  pp_role: $PPROLE
  pp_environment: $PPENV
  pp_zone: $PPZONE
EOF

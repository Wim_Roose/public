#!/bin/bash

# # bash -c "$(curl -fsSL https://bitbucket.org/Wim_Roose/public/raw/master/install_puppet_macos.sh)"

# https://puppet.com/docs/puppet/6/install_agents.html#install_mac_agents

CONFDIR="/etc/puppetlabs/puppet"
MACOS_VERSION=$(sw_vers | grep ProductVersion | awk {'print $2'} | cut -d . -f 1)

echo $MACOS_VERSION
read -r -p 'press key to continue'

uname -a | grep arm64
if [ $? == 1 ]; then
  echo "Downloading x86_64 version"
  curl -s https://downloads.puppet.com/mac/puppet7/$MACOS_VERSION/x86_64/puppet-agent-latest.dmg --output puppet-agent-latest.dmg
else
  echo "Downloading arm64 version"
  curl -s https://downloads.puppet.com/mac/puppet7/$MACOS_VERSION/arm64/puppet-agent-latest.dmg --output puppet-agent-latest.dmg
fi
#cd /Volumes/$(ls /Volumes/puppet-agent-*)
echo "Checking if puppet already installed"
if ! [ -f "/opt/puppetlabs/bin/puppet" ];then
  echo "Nope, mounting disk image & installing puppet" 
  mount | grep puppet || sudo hdiutil mount puppet-agent-latest.dmg
  cd /Volumes/puppet*
  sudo installer -pkg $(ls) -target /
  cd ~/
  sudo hdiutil unmount $(ls -d /Volumes/puppet*)
fi
echo "Puppet version:"
/opt/puppetlabs/bin/puppet --version
echo "Disabling puppet for a sec"
/opt/puppetlabs/bin/puppet agent --disable

[[ -f "$CONFDIR/puppet.conf" ]] && echo "Found existing puppet config, removing!"; rm -f "$CONFDIR/puppet.conf"

echo "Configuring puppet time!"

read -r -p "Certname: " CERTNAME
echo "certname= $CERTNAME" >> "$CONFDIR/puppet.conf"

read -r -p "IP address of puppet server: " IPADDRESS
sed -i 's/^.*puppet.*$//g' /etc/hosts
echo "$IPADDRESS puppet puppet.home" >> /etc/hosts

read -r -p "pp role: " PPROLE
read -r -p "pp env (prd/tst/dev): " PPENV
read -r -p "pp_zone: " PPZONE
cat <<EOF > "$CONFDIR/csr_attributes.yaml"
---
extension_requests:
  pp_role: $PPROLE
  pp_environment: $PPENV
  pp_zone: $PPZONE
EOF

mv /opt/puppetlabs/puppet/bin/wrapper.sh /opt/puppetlabs/puppet/bin/wrapper
ln -sf /opt/puppetlabs/puppet/bin/wrapper /opt/puppetlabs/bin/facter
ln -sf /opt/puppetlabs/puppet/bin/wrapper /opt/puppetlabs/bin/hiera
ln -sf /opt/puppetlabs/puppet/bin/wrapper /opt/puppetlabs/bin/puppet
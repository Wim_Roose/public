<#
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/Wim_Roose/public/raw/master/install_puppet.ps1'))


Originally developed by Hashicorp.
Source: https://raw.githubusercontent.com/hashicorp/puppet-bootstrap/master/windows.ps1

.SYNOPSIS
    Installs Puppet on this machine.

.DESCRIPTION
    Downloads and installs the official Puppet MSI package.

    This script requires administrative privileges.

    You can run this script from an old-style cmd.exe prompt using the
    following:

      powershell.exe -ExecutionPolicy Unrestricted -NoLogo -NoProfile -Command "& '.\puppet-agent-installer.ps1'"

.PARAMETER MsiUrl
    This is the URL to the Puppet MSI file you want to install. This defaults
    to a version from Puppet.

.PARAMETER PuppetVersion
    This is the version of Puppet that you want to install. If you pass this it will override the version in the MsiUrl.
    This defaults to $null.
#>
param(
    [string]$MsiUrl = "https://downloads.puppet.com/windows/puppet8/puppet-agent-x64-latest.msi",
    [string]$PuppetVersion = $null
)

if ($PuppetVersion) {
    if ($PuppetVersion[0] -eq '7') {
        $MsiUrl = "https://downloads.puppet.com/windows/puppet6/puppet-agent-$($PuppetVersion)-x64.msi"
    } else {
        if ($PuppetVersion[0] -eq '6') {
            $MsiUrl = "https://downloads.puppet.com/windows/puppet5/puppet-agent-$($PuppetVersion)-x64.msi"
        } else {
            $MsiUrl = "https://downloads.puppet.com/windows/puppet-agent-$($PuppetVersion)-x64.msi"
        }
    }
    Write-Output "Puppet version $PuppetVersion specified, updated MsiUrl to `"$MsiUrl`""
}

$PuppetInstalled = $false
try {
    $ErrorActionPreference = "Stop";
    Get-Command puppet | Out-Null
    $PuppetInstalled = $true
    $PuppetVersion = &puppet "--version"
    Write-Output "Puppet $PuppetVersion is installed. This process does not ensure the exact version or at least version specified, but only that puppet is installed. Exiting..."
} catch {
    Write-Output "Puppet is not installed, continuing..."
}

if (!($PuppetInstalled)) {
    $currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
    if (! ($currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))) {
        Write-Output -ForegroundColor Red "You must run this script as an administrator."
        Exit 1
    }

    # Install it - msiexec will download from the url
    $install_args = @("/qn", "/norestart", "/i", $MsiUrl)
    Write-Output "Installing Puppet. Running msiexec.exe $install_args"
    $process = Start-Process -FilePath msiexec.exe -ArgumentList $install_args -Wait -PassThru
    if ($process.ExitCode -ne 0) {
        Write-Output "Installer failed."
    }

    # Stop the service that it autostarts
    Write-Output "Stopping Puppet service that is running by default..."
    Start-Sleep -s 5
    Stop-Service -Name puppet

    Write-Output "Puppet successfully installed."
}
Write-Output "Let's define the certname for this little one"
$certname = Read-Host -Prompt 'certname'
Add-Content C:\ProgramData\PuppetLabs\puppet\etc\puppet.conf "certname=$certname"

Write-Output "Let's create a csr_attributes yaml file now!"
$pp_role = Read-Host -Prompt 'pp_role'
$pp_environment = Read-Host -Prompt 'pp_environment (dev/tst/prd)'
$pp_zone = Read-Host -Prompt 'pp_zone (bsb/nc/home)'
Set-Content C:\ProgramData\PuppetLabs\puppet\etc\csr_attributes.yaml "extension_requests:`n  pp_role: $pp_role`n  pp_environment: $pp_environment`n  pp_zone: $pp_zone"
$csr = Get-Content C:\ProgramData\PuppetLabs\puppet\etc\csr_attributes.yaml
Write-Output $csr

$ip = Read-Host -Prompt 'Puppetserver IP address'
Write-Output "Adding puppet to /etc/hosts"
Add-Content C:\Windows\System32\drivers\etc\hosts "$ip `t puppet `t puppet.home"

Write-Output "Refreshing environment now, so you can run puppet agent"
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")